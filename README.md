### Introduction
A simulation that shows how a simple elevator system works. The design is based on the paper called [A UML Documentation for an Elevator System by Lu Luo](http://www.cs.cmu.edu/~luluo/Courses/18540PhDreport.pdf "A UML Documentation for an Elevator System by Lu Luo"). Some subsystems have been omitted, along with some behaviour in the subsystems which are implemented.

The resulting document can be found in PassengerElevator_Structural_Overview_w_Diagrams (in different formats).