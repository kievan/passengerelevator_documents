#Functional Requirements for PassengerElevator

1. Process Car button presses: 
    - The elevator receives car calls from the passengers
    - Updates the record of car states in sequential manner
2. Process Floor button presses: 
    - Elevator receives hall calls from the passengers
    - Updates the record of car states in sequential manner
3. Move/Stop the Car: 
    - Conditions that make the car stop
    - Calculate driving directions of the car
4. Open/Close the Doors: 
    - The elevator should be able to open and close the doors for the 
      passengers to get in and out of the car. 

